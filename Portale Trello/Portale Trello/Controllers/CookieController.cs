﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Portale_Trello.Models.Etc;

namespace Portale_Trello.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    /* Da CookieController viene eliminato il suffisso "Controller" quindi per route e path /Cookie/Metodo */
    public class CookieController : ControllerBase
    {
        ILogger logger = Logger.Singleton();

        [HttpPost ("SignIn")]
        public async Task<ActionResult> SignInUser([FromForm] string username, [FromForm] string password)
        {
            logger.LogVerbose("Richiesta HTTP POST SignInUser per username: " + username);

            User user = await ResourceAccessorDatabase.Singleton().SingInUser(username, password);

            if (user != null)
            {
                List<Claim> claims = CookieAuth.BuildClaim(user);
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                AuthenticationProperties authenticationProperties = new AuthenticationProperties();

                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authenticationProperties);

                logger.Log("Utente trovato e Loggato, Cookie Authentication creato con successo");
            }
            else
            {
                logger.Log("Utente con username: " + username + " non trovato con la combinazione password inserita!");
            }

            return user == null ? Redirect("/SignInView/false") : Redirect("/SignInView/true");
        }





        [HttpGet ("SignOut")]
        public async Task<ActionResult> SignOutUser()
        {
            Claim username = HttpContext.User.Claims.FirstOrDefault(claim => claim.Type.Equals(ClaimTypes.Name));
            if (username != null)
            {
                logger.LogVerbose("Richiesta HTTP GET SignOutUser per username: " + username.Value);
                await HttpContext.SignOutAsync();
            }
            else
            {
                logger.LogVerbose("Richiesta HTTP GET SignOutUser, ma claim non esiste! Utente non loggato?");
            }
            return Redirect("/SignInView/SignOut");
        }
    }
}