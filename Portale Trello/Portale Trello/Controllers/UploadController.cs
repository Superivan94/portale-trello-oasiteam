﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Portale_Trello.Models.Trello;
using Portale_Trello.Models.Etc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System;
//NON IN USO
namespace Portale_Trello.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    /* Da CookieController viene eliminato il suffisso "Controller" quindi per route e path /Cookie/Metodo */
    public class UploadController : ControllerBase
    {
        [HttpPost("Link")]
        public async Task<ActionResult> UploadFile([FromForm] string api_key, [FromForm] string api_token, [FromForm] TrelloContext trelloContext, [FromForm] string cardId, [FromForm] string fileName, [FromForm] string url)
        {
            await trelloContext.AddAttachment(api_key, api_token, cardId, fileName, url);
            return Redirect("");
        }
    }
}
