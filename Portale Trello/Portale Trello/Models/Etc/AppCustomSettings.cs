﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace Portale_Trello.Models.Etc
{
    public static class AppCustomSettings
    {
        public const string FOLDER_DATA_NAME = "PortaleTrello";
        public const string FILE_CUSTOM_SETTINGS_NAME = "CustomSettings.txt";
        public static readonly string pathAppData;


        static AppCustomSettings()
        {
            try
            {
                //Costruisce il path C:\ProgramData\PortaleTrello
                pathAppData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\";
                pathAppData += Directory.CreateDirectory(pathAppData + FOLDER_DATA_NAME).Name + @"\";
                bool _exists = Directory.GetFiles(pathAppData).Contains<string>(pathAppData + FILE_CUSTOM_SETTINGS_NAME);
                if (!_exists)
                {
                    File.Create(pathAppData + FILE_CUSTOM_SETTINGS_NAME).Close();
                    string json = JsonSerializer.Serialize<AppCustomSettingsJson>(new AppCustomSettingsJson { trelloListsBusinessName = new string[1], trelloListsTechnicalName = new string[1] });
                    File.WriteAllText(pathAppData + FILE_CUSTOM_SETTINGS_NAME, json);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }




        #region Trello
        public static string[] ReadTrelloListsTechnicalName()
        {
            AppCustomSettingsJson settings = null;
            try
            {
                string appCustomSettingsJson = File.ReadAllText(pathAppData + FILE_CUSTOM_SETTINGS_NAME);
                settings = JsonSerializer.Deserialize<AppCustomSettingsJson>(appCustomSettingsJson);
            }
            catch (Exception e)
            {
                ILogger logger = Logger.Singleton();
                logger.LogError("Eccezione: " + e.StackTrace);
            }

            return settings.trelloListsTechnicalName;
        }




        public static string[] ReadTrelloListsBusinessName()
        {
            AppCustomSettingsJson settings = null;
            try
            {
                string appCustomSettingsJson = File.ReadAllText(pathAppData + FILE_CUSTOM_SETTINGS_NAME);
                settings = JsonSerializer.Deserialize<AppCustomSettingsJson>(appCustomSettingsJson);
            }
            catch (Exception e)
            {
                ILogger logger = Logger.Singleton();
                logger.LogError("Eccezione: " + e.StackTrace);
            }

            return settings.trelloListsBusinessName;
        }
        #endregion




        public class AppCustomSettingsJson
        {
            public string[] trelloListsTechnicalName { get; set; }
            public string[] trelloListsBusinessName { get; set; }
        }
    }
}