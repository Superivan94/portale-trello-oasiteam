﻿namespace Portale_Trello.Models.Etc
{
    public static class AppEndPoints
    {
        public const string HOST = "/_Host";
        public const string MESSAGE_HUB = "/messagesHub";
    }
}