﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Portale_Trello.Models.Etc
{
    public static class CookieAuth
    {
        public static List<Claim> BuildClaim(User user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.username),
                new Claim(ClaimTypes.Role, user.role),
                new Claim(User.LAST_CHANGED, user.last_changed.ToString()),
                new Claim(User.MODULE, user.module),
                new Claim(User.API_KEY, user.api_key == null ? "NotSet" : user.api_key),
                new Claim(User.API_TOKEN, user.api_token == null ? "NotSet" : user.api_token),
                new Claim(User.ID_TRELLO, user.id_trello == null ? "NotSet" : user.id_trello)
            };
            return claims;
        }
    }
}