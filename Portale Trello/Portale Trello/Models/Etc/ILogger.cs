﻿namespace Portale_Trello.Models.Etc
{
    interface ILogger
    {
        public void Log(string message);
        public void LogVerbose(string message);
        public void LogError(string message);
    }
}