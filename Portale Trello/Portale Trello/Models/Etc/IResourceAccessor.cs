﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Portale_Trello.Models.Etc
{
    public interface IResourceAccessor
    {
        public Task<bool> SingUpUser(User user);
        public Task<User> SingInUser(string username, string password);
        public Task<List<User>> GetUsersAsync();
    }
}