﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Database_Access_Layer;

namespace Portale_Trello.Models.Etc
{
    public class ResourceAccessorDatabase : IResourceAccessor
    {
        private string connectionStringDefault = "Server=127.0.0.1;Port=3306;database=oasiteam_app;user id=root;password=admin";
        private static IDatabaseAccess database;
        private static ResourceAccessorDatabase resourceAccessorDatabase;




        private ResourceAccessorDatabase() { }




        public static IResourceAccessor Singleton()
        {
            if (resourceAccessorDatabase == null)
            {
                resourceAccessorDatabase = new ResourceAccessorDatabase();
            }
            if (database == null)
            {
                database = new DatabaseAccessMySql();
            }

            return resourceAccessorDatabase;
        }




        public async Task<bool> SingUpUser(User user)
        {
            string sql = "INSERT INTO user (username, password, last_changed, role, module, api_key, api_token) VALUES (@username, @password, @last_changed, @role, @module, @api_key, @api_token);";
            var parameters = new { username = user.username, password = user.password, last_changed = DateTime.Now, role = UserPrivilegeLevels.READER, module = "", api_key = user.api_key, api_token = user.api_token };
            int rows = await database.SaveDataAsync(sql, parameters, connectionStringDefault);

            return rows >= 1;
        }




        public async Task<User> SingInUser(string username, string password)
        {
            string sql = "SELECT username, last_changed, role, module, api_key, api_token, id_trello  FROM user WHERE username = @username AND password = @password;";
            var parameters = new { username = username, password = password };
            List<User> user_found = await database.LoadDataAsync<User, dynamic>(sql, parameters, connectionStringDefault);

            return user_found.Count == 1 ? user_found[0] : null;
        }



        public async Task<List<User>> GetUsersAsync()
        {
            string sql = "SELECT * FROM user";
            List<User> users = await database.LoadDataAsync<User, dynamic>(sql, new { }, connectionStringDefault);
            return users;
        }
    }
}