﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using Portale_Trello.Models.Trello;

namespace Portale_Trello.Models.Etc
{
    public class ResourceAccessorLocalFiles : IResourceAccessor
    {
        //0 = LogVerbose, Log, LogError 
        //1 = Log, LogError
        //2 = LogError
        public const int DEBUG_LEVEL = 0;

        public const string FOLDER_DATA_NAME = "PortaleTrello";
        public string pathAppData;

        public string pathUsersFolder;

        public string LOG_FILE_NAME = "Log_";
        public string pathLogFolder;
        //File di log attualmente in uso
        public string pathLogFile;
        

        private static ResourceAccessorLocalFiles resourceAccessor;
        private static float logLineCounter = 0;


        private  ResourceAccessorLocalFiles()
        {
            try
            {
                //Costruisce il path C:\ProgramData\PortaleTrello
                this.pathAppData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\";
                this.pathAppData += Directory.CreateDirectory(this.pathAppData + FOLDER_DATA_NAME).Name + @"\";
            }
            catch (Exception)
            {
                throw;
            }

            this.InitLogFile();
            this.InitUsersFile();
        }


        public static ResourceAccessorLocalFiles Singleton()
        {
            if (resourceAccessor == null)
            {
                resourceAccessor = new ResourceAccessorLocalFiles();
            }
            return resourceAccessor;
        }


        /// <summary>
        /// Inizializza il file di log.
        /// </summary>
        public void InitLogFile()
        {
            try
            {
                this.pathLogFolder = Directory.CreateDirectory(this.pathAppData + @"\Logs\").FullName;

                //Trovo l'ultimo file di log Es: Log_1.txt, Log_2.txt ecc...
                string[] filesName = Directory.GetFiles(this.pathLogFolder, LOG_FILE_NAME + "*");
                int counter = 0;
                for (int _i = 0; _i < filesName.Length; _i++)
                {
                    string _substring = filesName[_i].Replace(this.pathLogFolder, "").Replace(LOG_FILE_NAME, "").Replace(".txt", "");
                    if (!int.TryParse(_substring, out int _counterOfFile))
                    {
                        throw new Exception(@"Errore nella creazione del file di LOG, controllare che tutti i file di Log siano nominati [Log_1.txt, Log_2.txt ecc] nella cartella C:\ProgramData\PortaleTrello\Logs");
                    }
                    counter = counter < _counterOfFile ? _counterOfFile : counter;
                }
                //Sappiamo che ultimo file di log si chiama Log_4, allora facciamo +1 e creaiamo il file di log Log_5.txt
                counter++;
                string fileLogName = LOG_FILE_NAME + Convert.ToString(counter);
                this.pathLogFile = this.pathLogFolder + fileLogName + ".txt";
                File.Create(this.pathLogFile).Close();
                this.Log("File di Log inizializzato correttamente. Livello Debug = " + DEBUG_LEVEL + ".");
            }
            catch (Exception error)
            {
                throw new Exception("Errore nella creazione del file di Log", error);
            }
        }


        private void InitUsersFile()
        {
            this.Log("Preparazione directory User");
            this.pathUsersFolder = this.pathAppData + @"\Users\";
            try
            {
                if (!Directory.Exists(this.pathUsersFolder))
                {
                    this.Log("Directory Users non trovato, creazione in corso...");
                    Directory.CreateDirectory(this.pathUsersFolder);
                    this.Log("Creazione directory Users completata con successo.");
                }
                else
                {
                    this.Log("Directory Users già presente.");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Scrive nel file di log. Debug level 0 e 1
        /// 
        /// Throw eccezioni che non hanno permesso la scrittura sul file
        /// </summary>
        /// <exception cref="Exception">Eccezioni che non hanno permesso la scrittura</exception>
        /// <param name="message">Messaggio da scrivere nel file di log</param>
        public void Log(string message)
        {
            if (DEBUG_LEVEL < 2)
            {
                string _line = Convert.ToString(logLineCounter);
                message = _line == "0" ? (_line + " | [LOG] | " + message) : ('\n' + _line + " | [LOG] | " + message);
                try
                {
                    File.AppendAllText(this.pathLogFile, message);
                }
                catch (Exception)
                {
                    throw;
                }
                logLineCounter++;
            }
        }

        /// <summary>
        /// Scrive nel file di log. Debug level 2
        /// 
        /// Throw eccezioni che non hanno permesso la scrittura sul file
        /// </summary>
        /// <exception cref="Exception">Eccezioni che non hanno permesso la scrittura</exception>
        /// <param name="message">Messaggio da scrivere nel file di log</param>
        public void LogError(string message)
        {
            string _line = Convert.ToString(logLineCounter);
            message = _line == "0" ? (_line + " | [ERROR] | " + message) : ('\n' + _line + " | [ERROR] | " + message);
            try
            {
                File.AppendAllText(this.pathLogFile, message);
            }
            catch (Exception)
            {
                throw;
            }
            logLineCounter++;
        }

        /// <summary>
        /// Scrive nel file di log. Debug level 0
        /// 
        /// Throw eccezioni che non hanno permesso la scrittura sul file
        /// </summary>
        /// <exception cref="Exception">Eccezioni che non hanno permesso la scrittura</exception>
        /// <param name="message">Messaggio da scrivere nel file di log</param>
        public void LogVerbose(string message)
        {
            if (DEBUG_LEVEL == 0)
            {
                string _line = Convert.ToString(logLineCounter);
                message = _line == "0" ? (_line + " | [VERBOSE] | " + message) : ('\n' + _line + " | [VERBOSE] | " + message);
                try
                {
                    File.AppendAllText(this.pathLogFile, message);
                }
                catch (Exception)
                {
                    throw;
                }
                logLineCounter++;
            }
        }

        public void SingupUser(User user)
        {
            //Carica file testo criptati con info degli users con GetUsers()
            List<User> users = GetUsers();
            //Controlla che non sia già presente
            //N.B. Nickname funge da chiave univoca
            bool exist = users.Contains<User>(user);
            if (!exist)
            {
                users.Add(user);
                //Aggiorna la lista utenti
                try
                {
                    File.Create(this.pathUsersFolder + user.username + ".txt").Close();
                    string _userJson = JsonSerializer.Serialize<User>(user);
                    File.WriteAllText(this.pathUsersFolder + user.username + ".txt", _userJson);
                }
                catch (Exception)
                {
                    throw;
                }
            }


            //TODO creare e caricare al volo un file di settings
            //Scrive file di log
        }

        public void SinginUser(User user)
        {
            //carica file GetUsers()
            //controlla che esiste
            //controlla password
            //stato di loggato
            //Scrive file di log
        }
        //TODO implementare criptare

        public List<User> GetUsers()
        {
            //Carica file tutti user
            string[] filesName = Directory.GetFiles(this.pathUsersFolder);
            List<User> users = new List<User>();
            for (int _i = 0; _i < filesName.Length; _i++)
            {
                string _userData = File.ReadAllText(filesName[_i]);
                LogVerbose("Utente letto: " + _userData);
                users.Add(JsonSerializer.Deserialize<User>(_userData));
            }

            return users;
        }
    }
}