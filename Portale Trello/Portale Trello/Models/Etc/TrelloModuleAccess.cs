﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portale_Trello.Models.Etc
{
    public class TrelloModuleAccess
    {
        public const string VIEW_TUTTE_LE_LISTE = "V_Tutte_Le_Liste";
        public const string VIEW_SCHEDE_PER_LISTA = "V_Schede_Per_Lista";
        public const string BUTTON_APRI_IN_TRELLO = "B_Apri_In_Trello";
        public const string BUTTON_CREA_APPUNTAMENTO = "B_Crea_Appuntamento";
        public const string BUTTON_OSPITANTE = "B_Ospitante";
        public const string BUTTON_ALLEGATO = "B_Allegato";
        public const string BUTTON_PARTNER = "B_Partner";
        public const string BUTTON_PUSH = "B_Push";
        public const string BUTTON_PRIVACY = "B_Privacy";
        public const string BUTTON_SQUADRA = "B_Squadra";
        public const string BUTTON_CAMBIA_STATO_APPUNTAMENTO = "B_Cambia_Stato_Appuntamento";
    }
}
