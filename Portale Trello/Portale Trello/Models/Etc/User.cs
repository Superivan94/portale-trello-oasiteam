﻿using System;

namespace Portale_Trello.Models.Etc
{
    public class User
    {
        public string username { get; set; }
        public string password { get; set; }
        public string passwordRepeated { get; set; }
        public string role { get; set; }
        public string module { get; set; }
        public DateTime last_changed { get; set; }
        public string api_key { get; set; }
        public string api_token { get; set; }
        public string id_trello { get; set; }

        public const string LAST_CHANGED = "last_changed";
        public const string MODULE = "module";
        public const string API_KEY = "api_key";
        public const string API_TOKEN = "api_token";
        public const string ID_TRELLO = "id_trello";
        

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (obj is not User) { return false; }
            if (username.Equals(((User)obj).username))
            {
                return true;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            //Per combinare più valori usa operatore ^ [Es: var1.GetHashCode() ^ var2.GetHashCode()]
            return username == null ? 0 : username.GetHashCode();
        }
    }
}