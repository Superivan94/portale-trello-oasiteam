﻿namespace Portale_Trello.Models.Etc
{
    public static class UserModulesAccess
    {
        public const string TRELLO = "Trello";
        public const string OASIFORYOU = "oasiforyou";
        public const string TICKET_READER = "ticket";
    }
}