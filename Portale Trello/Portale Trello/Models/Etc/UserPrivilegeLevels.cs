﻿namespace Portale_Trello.Models.Etc
{
    public static class UserPrivilegeLevels
    {
        //Tutto
        public const string ADMIN = "admin";
        //Tutto meno amministrazione APP
        public const string COORDINATOR = "coordinator";
        //Lettura e/o Scrittura Moduli (Trello, Ticket ecc...)
        public const string OASITEAM = "oasiteam";
        //Lettura e/o Scrittura Moduli (Trello, Ticket ecc...)
        public const string PARTNER = "partner";
        //Lettura Moduli (Principalmente Ticket ecc...)
        public const string CUSTOMER = "customer";
        //Lettura Moduli (Principalmente Ticket ecc...)
        public const string READER = "reader";
    }
}