﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Portale_Trello.Models.Hubs
{
    public class MessagesHub : Hub
    {
        public async Task NotificationChangesToAll(string listId, string cardId)
        {
            await Clients.All.SendAsync("UpdateView", listId, cardId);
        }
    }

    public static class MessagesHubSend
    {
        public const string NotificationChangesToAll = "NotificationChangesToAll";
    }
    
    public static class MessagesHubReceive
    {
        public const string UpdateView = "UpdateView";
    }
}