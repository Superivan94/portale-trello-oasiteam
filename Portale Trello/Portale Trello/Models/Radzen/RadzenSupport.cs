﻿//Autore: Ivan Degiorgis
using Radzen;

namespace Portale_Trello.Models.Radzen
{
    public static class RadzenSupport
    {
        public static void SendNotificationMessage(NotificationService notificationServiceIstance, NotificationSeverity notificationSeverity, string summary, string detail, int duration = 4000)
        {
            NotificationMessage notificationMessage = new NotificationMessage
            {
                Severity = notificationSeverity,
                Summary = summary,
                Detail = detail,
                Duration = duration
            };
            notificationServiceIstance.Notify(notificationMessage);
        }
    }
}