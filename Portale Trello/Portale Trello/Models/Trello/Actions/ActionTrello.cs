﻿using System;

namespace Portale_Trello.Models.Trello.Actions
{
    public class ActionTrello
    {
        public string id { get; set; }
        public string idMemberCreator { get; set; }
        public Data data { get; set; }
        public string type { get; set; }
        public DateTime date { get; set; }
        public MemberCreator memberCreator { get; set; }
    }
}