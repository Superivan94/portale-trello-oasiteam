﻿namespace Portale_Trello.Models.Trello.Actions
{
    public class Data
    {
        public string text { get; set; }
        public Card card { get; set; }
        public Board board { get; set; }
        public List list { get; set; }
    }
}