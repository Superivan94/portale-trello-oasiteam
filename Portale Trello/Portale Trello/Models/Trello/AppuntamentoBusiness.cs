﻿using System;

namespace Portale_Trello.Models.Trello
{
    public class AppuntamentoBusiness
    {
        public DateTime start;
        public DateTime end;
        public DateTime creationDate;
        public string state;
        public int number;


        public override string ToString()
        {
            return "Per il giorno " + start.ToString() + " | Preso il " + creationDate.ToString();
        }
    }
}
