﻿using System;
using System.Collections.Generic;

namespace Portale_Trello.Models.Trello
{
    public class Attachment
    {
        public string id { get; set; }
        public int? bytes { get; set; }
        public DateTime date { get; set; }
        public string edgeColor { get; set; }
        public string idMember { get; set; }
        //Aggiunto io per comodità
        public Member memberCreator {  get; set; } 
        public bool isUpload { get; set; }
        public string mimeType { get; set; }
        public string name { get; set; }
        public List<Preview> previews { get; set; }
        public string url { get; set; }
        public int pos { get; set; }
        public string fileName { get; set; }
    }

    public class Preview
    {
        public string id { get; set; }
        public string _id { get; set; }
        public bool scaled { get; set; }
        public string url { get; set; }
        public int bytes { get; set; }
        public int height { get; set; }
        public int width { get; set; }
    }
}
