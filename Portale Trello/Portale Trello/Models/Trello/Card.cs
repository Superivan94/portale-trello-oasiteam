﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Portale_Trello.Models.Trello.Plugins.Crmble;
using Portale_Trello.Models.Trello.Plugins.Crmble.Rest;

namespace Portale_Trello.Models.Trello
{
    public class Card
    {
        #region Variabili REST
        public string id { get; set; }
        //Indica se Attiva o Archiviata (closed)
        public bool closed { get; set; }
        public string dateLastActivity { get; set; }
        public string desc { get; set; }
        public string name { get; set; }
        public bool isTemplate { get; set; }
        public bool dueComplete { get; set; }
        public string due { get; set; }
        //Inizio della dueDate, mentre due indica la fine (due variabile principale)
        public string start { get; set; }
        public List<string> idMembers { get; set; }
        public List<Label> labels { get; set; }
        public string shortUrl { get; set; }
        public string idList { get; set; }
        public List<CardFieldPluginData> pluginData { get; set; }
        #endregion

        #region Variabili Business
        protected PluginValueCrmble pluginValueCrmble { get; set; }
        public AppuntamentoBusiness[] appuntamenti { get; set; }
        /*
            È identico a "string due", ma serve quando questi dati vengono visualizzati
            nella tabella di RadzenUI per consentire il corretto funzionamento dei filtri
            di ordinamento e ricerca.
        */
        public DateTime dueDateTime { get; set; }
        #endregion


        public PluginValueCrmble GetPluginValueCrmble()
        {
            if (pluginValueCrmble == null)
            {
                CardFieldPluginData crmble = pluginData.Find(plugin => plugin.idPlugin == TrelloContext.crmbleId);
                CrmbleRootObject rootObj = JsonSerializer.Deserialize<CrmbleRootObject>(crmble.value);
                List<CrmbleField> crmbleFields = rootObj.crmbleCardContact.crmbleFieldsValues;

                pluginValueCrmble = new PluginValueCrmble(crmbleFields);
            }

            return pluginValueCrmble;
        }
    }
}