﻿namespace Portale_Trello.Models.Trello
{
    public class CardFieldPluginData
    {
        public string id { get; set; }
        public string idPlugin { get; set; }
        public string scope { get; set; }
        public string idModel { get; set; }
        public string value { get; set; }
        public string access { get; set; }
        public string dateLastUpdate { get; set; }
    }
}