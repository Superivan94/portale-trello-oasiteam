﻿using System;

namespace Portale_Trello.Models.Trello
{
    public class Label : IComparable<Label>
    {
        public string id { get; set; }
        public string idBoard { get; set; }
        public string name { get; set; }
        public string color { get; set; }
        public enum LabelColor
        {
            Green,
            Yellow,
            Orange,
            Red,
            Purple,
            Blue,
            Sky,
            Lime,
            Pink,
            Black,
            Null
        }

        public int CompareTo(Label other)
        {
            int c_1 = GetColorCode(this.color);
            int c_2 = GetColorCode(other.color);

            return c_1 - c_2;
        }

        public int GetColorCode(string color_name)
        {
            int color_code = -1;

            switch (color_name)
            {
                case "green":
                    color_code = 1;
                    break;
                case "yellow":
                    color_code = 2;
                    break;
                case "orange":
                    color_code = 3;
                    break;
                case "red":
                    color_code = 4;
                    break;
                case "purple":
                    color_code = 5;
                    break;
                case "blue":
                    color_code = 6;
                    break;
                case "sky":
                    color_code = 7;
                    break;
                case "lime":
                    color_code = 8;
                    break;
                case "pink":
                    color_code = 9;
                    break;
                case "black":
                    color_code = 10;
                    break;
                case null:
                    color_code = 11;
                    break;
            }

            return color_code;
        }
    }
}