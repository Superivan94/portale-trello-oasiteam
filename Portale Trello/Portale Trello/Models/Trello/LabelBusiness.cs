﻿namespace Portale_Trello.Models.Trello
{
    //N.B. se vengono eliminate e ricreate da Trello cambia id
    public static class LabelBusiness
    {
        public const string SENZA_RELAZIONE_CONTATTO    = "5f9adbf71d1ebd158ef58ad8";
        public const string SOSPESO                     = "5ff48e7afa7c6c4271781023";
        public const string KO_TECNICO                  = "5ff6f0cafd688934c09c42f1";
        public const string ANNULLATO                   = "5ff6f12f508b3272936ad3b7";
        public const string RIFIUTATO                   = "5ff6ebeec3aac64268f41ec4";

        public const string CONTATTO                    = "5f9a80848f9e114d29b2c115";
        public const string OPPORTUNITA                 = "5f9a874ce2d1cf487ab56efa";
        public const string COMMESSA                    = "5f9a9dfd7939046e405a1f11";

        public const string PUSH_ACCETTA                = "60bf908648f6047724b067bc";
        public const string PUSH_RISENTIRE              = "60c224169d4baf357fbb36f3";
        public const string PUSH_NON_ACCETTA            = "60bf90aa79a73a350ee9a6f0";

        public const string PRIVACY_ACCETTA             = "60ca19fb402be279298404e2";
        public const string PRIVACY_NON_ACCETTA         = "60ca1a001ba24b31500dcf75";

        public const string MICSO                       = "603cacdd41dab4575c390464";
        public const string SKYQ                        = "6040afaac342e57b504154d9";
        public const string SKY                         = "60424a2d0e859c73c4c76363";
        public const string TIMENET                     = "6040c0305f3f3645cfb4d7e4";
        public const string ISABELL                     = "6040c027370032776211e8ea";
        public const string BIGBLUE                     = "6048d94e97ba9082a8d5dab8";
        public const string ALPSIM                      = "605b177b774dba0f77d59106";
        public const string PASER                       = "6040c03aa6b3928b7c4e61fd";

        public const string VENDITORE                   = "60e2d5726e4f580a331c36f4";
        public const string OSPITANTE_BTS               = "60d7722cd70b1273d5c499df";
        public const string PARTNER                     = "60d7722cd70b1273d5c499df";
        public const string SQUADRA_1                   = "60ca1987f18ef62baa56b624";
        public const string SQUADRA_2                   = "60ca196aa4915857d909b98a";
        public const string FAQ                         = "6048a2e7e7beb2366a61de9d";
        public const string ATTENZIONE                  = "604fbff0117a1c4bb3f4a32c";
        public const string DA_TESTARE                  = "60425723e1d029222531130e";
    }
}