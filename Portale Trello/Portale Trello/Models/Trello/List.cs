﻿namespace Portale_Trello.Models.Trello
{
    public class List
    {
        #region Variabili REST
        public string id { get; set; }
        public string name { get; set; }
        public bool closed { get; set; }
        public float pos { get; set; }
        public string softLimit { get; set; }
        public string idBoard { get; set; }
        public bool subscribed { get; set; }
        //Fa distinzione tra le liste considerate "Tecniche" (Admin) e quelle "Business" (Utenti)
        public bool isTypeBusiness { get; set; }
        #endregion

        #region Variabili Business
        public string description { get; set; }
        #endregion
    }
}