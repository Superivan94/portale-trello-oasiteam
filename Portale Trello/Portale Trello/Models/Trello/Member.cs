﻿using System.Collections.Generic;

namespace Portale_Trello.Models.Trello
{
    public class Member
    {
        public string id { get; set; }
        public bool activityBlocked { get; set; }
        public string avatarHash { get; set; }
        public string avatarUrl { get; set; }
        public string bio { get; set; }
        public object bioData { get; set; }
        public bool confirmed { get; set; }
        public string fullName { get; set; }
        public object idEnterprise { get; set; }
        public string memberType { get; set; }
        public string url { get; set; }
        public string username { get; set; }
        public string status { get; set; }
        public string avatarSource { get; set; }
        public string email { get; set; }
        public string gravatarHash { get; set; }
        public List<string> idBoards { get; set; }
        public List<string> idOrganizations { get; set; }
    }
}
