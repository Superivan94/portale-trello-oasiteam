﻿namespace Portale_Trello.Models.Trello.Plugins.Crmble
{
    public class CrmbleFieldsId
    {
        /*
        Questo oggetto viene usato per estrapolare i dati necessari. Tramite API Rest
        arriva una lista di oggetti "Field" composti da un "ID" e un "VALORE", questi sono
        gli "ID" generati per la bacheca Trello CRM OasiTeam.
        


        !!ATTENZIONE!!
        Per colpa degli aggiornamenti fatti "male" da parte degli sviluppatori
        di questo plug-in alcuni campi hanno degli ID che traggono in errore.

        Per esempio alcuni campi predefiniti vengono riusati come:
        Il campo per il valore "Cellulare" ha come ID "jobTitle". Quindi per leggere il
        valore del cellulare bisogna cercare il campo con ID "jobTitle". 
        */
        public const string Nome = "firstName";
        public const string Cognome = "lastName";
        public const string TelefonoFisso = "phone";
        //ATTENZIONE
        public const string Cellulare = "jobTitle";
        public const string Email = "email";
        public const string EmailPec = "company";
        public const string Fonte = "origin";
        public const string DataCreazione = "dateCreated";
        public const string AziendaNome = "_97ae";
        public const string PartitaIva = "_82bc";
        public const string CodiceFiscale = "_b5ef";
        public const string Indirizzo = "_840c";
        public const string NumeroCivico = "_bb00";
        public const string Citta = "_9969";
        public const string Provincia = "_afa3";
        public const string Frazione = "_8ab0";
        public const string Cap = "_972b";
        public const string MicsoCodice = "_8934";
        public const string CodiceUnivoco = "_bb20";
    }
}