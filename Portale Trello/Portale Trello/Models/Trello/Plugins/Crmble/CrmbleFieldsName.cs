﻿namespace Portale_Trello.Models.Trello.Plugins.Crmble
{
    public class CrmbleFieldsName
    {
        /*
        Questo oggetto viene usato per dare un nome informale ai
        campi estrapolati dal plugin "Crmble" invece che i loro "ID" logici...
         */
        public const string Nome = "Nome";
        public const string Cognome = "Cognome";
        public const string TelefonoFisso = "Telefono Fisso";
        public const string Cellulare = "Cellulare";
        public const string Email = "Email";
        public const string EmailPec = "Email PEC";
        public const string Fonte = "Fonte";
        public const string DataCreazione = "Data Creazione";
        public const string AziendaNome = "Nome Azienda";
        public const string PartitaIva = "Partita IVA";
        public const string CodiceFiscale = "Codice Fiscale";
        public const string Indirizzo = "Indirizzo";
        public const string NumeroCivico = "Numero Civico";
        public const string Citta = "Città";
        public const string Provincia = "Provincia";
        public const string Frazione = "Frazione";
        public const string Cap = "CAP";
        public const string MicsoCodice = "Codice Micso";
        public const string CodiceUnivoco = "Codice Univoco";
    }
}