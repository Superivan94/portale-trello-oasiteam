﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Portale_Trello.Models.Etc;
using Portale_Trello.Models.Trello.Plugins.Crmble.Rest;

namespace Portale_Trello.Models.Trello.Plugins.Crmble
{
    public class PluginValueCrmble
    {
        /*
        public string nome { get; set; }
        public string cognome { get; set; }
        public string telefonoFisso { get; set; }
        public string cellulare { get; set; }
        public string email { get; set; }
        public string emailPec { get; set; }
        public string source { get; set; }
        public DateTime dataCreazione { get; set; }
        public string aziendaNome { get; set; }
        public string partitaIva { get; set; }
        public string codiceFiscale { get; set; }
        public string indirizzo { get; set; }
        public string numeroCivico { get; set; }
        public string citta { get; set; }
        public string provincia { get; set; }
        public string frazione { get; set; }
        public string cap { get; set; }
        public string micsoCodice { get; set; }
        //Forse il codice partner rilasciato da micso per i collaboratori???
        public string codiceUnivoco { get; set; }
        */

        public Dictionary<string, string> fields;



        public PluginValueCrmble(List<CrmbleField> crmbleFields)
        {
            fields = new Dictionary<string, string>();

            fields.Add(CrmbleFieldsName.Nome, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Nome)).value);
            fields.Add(CrmbleFieldsName.Cognome, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Cognome)).value);
            fields.Add(CrmbleFieldsName.TelefonoFisso, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.TelefonoFisso)).value);
            fields.Add(CrmbleFieldsName.Cellulare, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Cellulare)).value);
            fields.Add(CrmbleFieldsName.Email, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Email)).value);
            fields.Add(CrmbleFieldsName.EmailPec, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.EmailPec)).value);
            fields.Add(CrmbleFieldsName.Fonte, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Fonte)).value);
            fields.Add(CrmbleFieldsName.DataCreazione, PluginValueCrmble.DateParseFromRest(crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.DataCreazione)).value).ToString());
            fields.Add(CrmbleFieldsName.AziendaNome, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.AziendaNome)).value);
            fields.Add(CrmbleFieldsName.PartitaIva, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.PartitaIva)).value);
            fields.Add(CrmbleFieldsName.CodiceFiscale, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.CodiceFiscale)).value);
            fields.Add(CrmbleFieldsName.Indirizzo, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Indirizzo)).value);
            fields.Add(CrmbleFieldsName.NumeroCivico, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.NumeroCivico)).value);
            fields.Add(CrmbleFieldsName.Citta, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Citta)).value);
            fields.Add(CrmbleFieldsName.Provincia, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Provincia)).value);
            fields.Add(CrmbleFieldsName.Frazione, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Frazione)).value);
            fields.Add(CrmbleFieldsName.Cap, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.Cap)).value);
            fields.Add(CrmbleFieldsName.MicsoCodice, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.MicsoCodice)).value);
            fields.Add(CrmbleFieldsName.CodiceUnivoco, crmbleFields.Find(field => field.id.Equals(CrmbleFieldsId.CodiceUnivoco)).value);
        }



        public static DateTime DateParseFromRest(string date)
        {
            date = date.Replace("(Ora standard dell’Europa centrale)", "").Trim();
            DateTime result = new DateTime();
            bool is_success = DateTime.TryParseExact(date, "ddd MMM dd yyyy HH:mm:ss 'GMT'K", CultureInfo.InvariantCulture, DateTimeStyles.None, out result);

            if (!is_success)
            {
                ILogger logger = Logger.Singleton();
                logger.LogError("Impossibile convertire Data Creazione: " + date +
                    " da Plugin Crmble API REST nel formato ddd MMM dd yyyy HH:mm:ss 'GMT'");
            }

            return result;
        }




        public Dictionary<string, string> GetFieldsNotNullOrEmpty()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> field in fields)
            {
                if (!String.IsNullOrEmpty(field.Value.Trim()))
                {
                    result.Add(field.Key, field.Value);
                }
            }

            return result;
        }
    }
}