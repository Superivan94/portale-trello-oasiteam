﻿using System;
using System.Collections.Generic;

namespace Portale_Trello.Models.Trello.Plugins.Crmble.Rest
{
    public class CrmbleCardContact
    {
        public List<CrmbleField> crmbleFieldsValues { get; set; }
        public List<object> googleUrls { get; set; }
        public bool googleDrive { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime dateLastUpdate { get; set; }
        public CrmbleList crmbleList { get; set; }
        public string cardTitle { get; set; }
        public string description { get; set; }
        public string trelloUrl { get; set; }
        public string trelloCardId { get; set; }
        public string trelloBoardName { get; set; }
    }
}