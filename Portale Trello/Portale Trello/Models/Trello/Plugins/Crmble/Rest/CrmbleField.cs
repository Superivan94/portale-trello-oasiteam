﻿namespace Portale_Trello.Models.Trello.Plugins.Crmble.Rest
{
    public class CrmbleField
    {
        public string id { get; set; }
        public string value { get; set; }
    }
}