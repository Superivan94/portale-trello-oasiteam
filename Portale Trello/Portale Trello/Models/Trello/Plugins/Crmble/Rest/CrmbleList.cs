﻿namespace Portale_Trello.Models.Trello.Plugins.Crmble.Rest
{
    public class CrmbleList
    {
        public string id { get; set; }
        public int pos { get; set; }
        public string color { get; set; }
        public bool showInReports { get; set; }
        public bool showInFunnel { get; set; }
        public string name { get; set; }
    }
}