﻿using System.Text.Json.Serialization;

namespace Portale_Trello.Models.Trello.Plugins.Crmble.Rest
{
    public class CrmbleRootObject
    {
        [JsonPropertyName("CRMBLE_CARD_CONTACT")]
        public CrmbleCardContact crmbleCardContact { get; set; }
    }
}