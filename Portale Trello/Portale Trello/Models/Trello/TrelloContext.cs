﻿using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;
using Portale_Trello.Models.Etc;
using Portale_Trello.Models.Trello.Actions;
using Portale_Trello.Services.Trello;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Portale_Trello.Models.Trello
{
    /*
    Refactor per una specie di caches
    */
    public class TrelloContext : TrelloService
    {
        public const string boardId = "qsSRWiRN";
        public const string crmbleId = "5e5e8b4b55a4ea5fedc0dafa";


        #region Board
        public async Task<Label[]> GetLabelsBoard(IEnumerable<Claim> claims)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            return await HttpGetLabels(api_key, api_token);
        }




        public async Task<Member> GetMember(IEnumerable<Claim> claims, string memberId = null)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            if (memberId == null) { memberId = claims.First(claim => claim.Type.Equals(User.ID_TRELLO)).Value; }
            return await HttpGetMember(api_key, api_token, memberId);
        }
        #endregion

        #region List
        public async Task<List<List>> GetLists(IEnumerable<Claim> claims)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            List<List> lists = (await HttpGetLists(api_key, api_token)).ToList();
            lists.ForEach(list =>
            {
                list.isTypeBusiness = AppCustomSettings.ReadTrelloListsBusinessName()
                    .Contains(list.name);
            });

            return lists;
        }




        public async Task<Card[]> GetCardsPerList(IEnumerable<Claim> claims, string listId)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            return await HttpGetCardsPerList(api_key, api_token, listId);
        }




        public List<List> ListAddDescription(List<List> lists)
        {
            for (int _i = 0; _i < lists.Count; _i++)
            {
                switch (lists[_i].name)
                {
                    case "Nuovi Contatti":
                        lists[_i].description = "Contiene solo contatti che sono stati registrati recentemente "
                            + "o confermati da vecchie importazioni.";
                        break;
                    case "Contatti":
                        lists[_i].description = "Contiene tutti i contatti tranne le recenti aggiunte.";
                        break;
                    case "1. Opportunità":
                        lists[_i].description = "Contiene solo le opportunità. Le opportunità sono una possibilità di guadagno, "
                            + "nulla è ancora finalizzato, ma è stato mostrato interesse, da parte del cliente, verso un servizio o prodotto. "
                            + "Nelle fasi successive come la conferma di un preventivo si trasforma in una commessa.";
                        break;
                    case "2. Commesse":
                        lists[_i].description = "Contiene solo commesse. Sono tutte le nuove opportunità che stanno per finalizzare una vendita, "
                            + "solitamente in questa fase sono previsti appuntamenti in loco dal cliente per installazioni hardware ed altro tipo.";
                        break;
                    case "3. Commesse Completate":
                        lists[_i].description = "Contiene solo commesse da considerate completate, ovvero una commessa in cui sia stata confermata la fatturazione.";
                        break;
                    case "Commesse Annullato//KO":
                        lists[_i].description = "Contiene solo le commesse che per vari motivi sono in uno stato di blocco, in attesa di un'azione di recupero o di scarto.";
                        break;
                    default:
                        break;
                }
            }

            return lists;
        }
        #endregion

        #region Card
        public async void SetDueComplete(IEnumerable<Claim> claims, Card card)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            await HttpPutDueComplete(api_key, api_token, card.id, card.dueComplete);
        }




        public async void SetDueDate(IEnumerable<Claim> claims, Card card)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            await HttpPutDueDate(api_key, api_token, card.id, card.due, card.start);
        }




        public Card[] CardDataFormatChange(Card[] cardsToChangeFormat)
        {
            List<Card> cardsFormatted = new List<Card>();
            foreach (Card card in cardsToChangeFormat)
            {
                cardsFormatted.Add(CardDataFormatChange(card));
            }

            return cardsFormatted.ToArray();
        }




        public Card CardDataFormatChange(Card cardToChangeFormat)
        {
            #region Name
            cardToChangeFormat.name = cardToChangeFormat.name.Replace("CNT -", "").Trim();
            #endregion

            #region Labels
            /*
            Le labels (etichette) vengono inserite da Trello in ordine cronologico, ma poi vengono visulizzate
            secondo un ordine alfabetico e di colore. Questa regione cerca di dare un ordine costante alle labels delle varie schede.
            Per esempio quando si visulizzano le schede "Contatto" con l'etichetta "Contatto" l'obiettivo e di
            visualizzare la label "Contatto" sempre come prima tra le altre.

            N.B. Attualmente viene rimossa l'etichetta in base al tipo di lista visualizzata, per esempio:
                 se viene visualizzata la lista "Contatti" la label "Contatto" viene rimossa.
            */
            cardToChangeFormat.labels.Sort();

            int label_index = cardToChangeFormat.labels.FindIndex(label =>
            {
                bool match = false;
                if (label.name.Equals("Contatto") || label.name.Equals("Opportunità") || label.name.Equals("Commessa"))
                {
                    match = true;
                }
                return match;
            });

            if (label_index != -1)
            {
                Label label_to_move = cardToChangeFormat.labels[label_index];
                cardToChangeFormat.labels.RemoveAt(label_index);
                //card.labels.Insert(0, label_to_move);
            }


            /*
            Nel caso di label senza colore Trello le visualizza solo quando si apre una scheda, di default hanno
            un colore di background bianco e in <span> il testo e bianco, quindi cambio colore di background.
            */
            cardToChangeFormat.labels.ForEach(label =>
            {
                if (label.color == null)
                {
                    label.color = "gray";
                }
            });
            #endregion

            #region Data
            DateTime data = new DateTime();
            DateTime.TryParse(cardToChangeFormat.due, out data);
            if (!data.ToString().Equals(@"01/01/0001 00:00:00"))
            {
                cardToChangeFormat.due = data.ToString();
                cardToChangeFormat.dueDateTime = data;
            }
            else
            {
                cardToChangeFormat.due = "Non presente";
            }
            #endregion

            #region Description
            /*
            Formato della descrizione come esempio:
            "Note aggiuntive...

            N.B. *La sezione degli appuntamenti deve trovarsi in fondo alla descrizione.*

            1. Per il giorno dd/MM/yyyy, HH:mm | Preso il dd/MM/yyyy, HH:mm

            1. Per il giorno dd/MM/yyyy, HH:mm | Preso il dd/MM/yyyy, HH:mm
            ###Sospeso

            1. Per il giorno dd/MM/yyyy, HH:mm | Preso il dd/MM/yyyy, HH:mm
            */

            cardToChangeFormat.desc = cardToChangeFormat.desc.Replace("Note aggiuntive...", "");
            cardToChangeFormat.desc = cardToChangeFormat.desc.Replace("\n---", "");
            cardToChangeFormat.desc = cardToChangeFormat.desc.Replace("N.B. *La sezione degli appuntamenti deve trovarsi in fondo alla descrizione.*", "");
            cardToChangeFormat.desc = cardToChangeFormat.desc.Trim();

            int indexApp = cardToChangeFormat.desc.IndexOf("1. Per il giorno ");
            if (indexApp != -1)
            {
                string apps = cardToChangeFormat.desc.Substring(indexApp);
                apps = apps.Replace("1.", "!1.");
                string[] _ = apps.Split("!");
                //Rimuovo il primo split che vale ""
                //Quindi ricreo array pulito
                string[] appsSplitted = new string[_.Length - 1];
                for (int _i = 0; _i < appsSplitted.Length; _i++)
                {
                    appsSplitted[_i] = _[_i + 1];
                }

                cardToChangeFormat.appuntamenti = new AppuntamentoBusiness[appsSplitted.Length];
                for (int _i = 0; _i < appsSplitted.Length; _i++)
                {
                    appsSplitted[_i] = appsSplitted[_i].Trim();

                    string app = appsSplitted[_i];
                    string state = "Ok";
                    if (app.Contains("###Sospeso"))
                    {
                        app = app.Replace("###Sospeso", "").Trim();
                        state = "Sospeso";
                    }
                    else if (app.Contains("###Spostato"))
                    {
                        app = app.Replace("###Spostato", "").Trim();
                        state = "Spostato";
                    }
                    else if (app.Contains("###Annullato"))
                    {
                        app = app.Replace("###Annullato", "").Trim();
                        state = "Annullato";
                    }

                    //46 sono i caratteri della parte finale che mi interessa
                    //ovvero dd/MM/yyyy, HH:mm | Preso il dd/MM/yyyy, HH:mm
                    app = app.Substring(app.Length - 46);
                    app = app.Replace("Preso il", "");
                    app = app.Replace(",", "");
                    string[] subSplit = app.Split("|");

                    for (int _j = 0; _j < subSplit.Length; _j++)
                    {
                        subSplit[_j] = subSplit[_j].Trim();
                    }

                    cardToChangeFormat.appuntamenti[_i] = new AppuntamentoBusiness();
                    if (!DateTime.TryParse(subSplit[0], out cardToChangeFormat.appuntamenti[_i].start))
                    {
                        state = "Errore";
                        try
                        {
                            cardToChangeFormat.appuntamenti[_i].start = DateTime.Parse(subSplit[0]);
                        }
                        catch (Exception e)
                        {
                            ILogger logger = Logger.Singleton();
                            logger.LogError("Errore nella conversione della data di appuntamento: " + e.StackTrace);
                        }
                    }
                    if (!DateTime.TryParse(subSplit[1], out cardToChangeFormat.appuntamenti[_i].creationDate))
                    {
                        state = "Errore!!!!";
                        try
                        {
                            cardToChangeFormat.appuntamenti[_i].creationDate = DateTime.Parse(subSplit[1]);
                        }
                        catch (Exception e)
                        {
                            ILogger logger = Logger.Singleton();
                            logger.LogError("Errore nella conversione della data di creazione appuntamento: " + e.StackTrace);
                        }
                    }
                    cardToChangeFormat.appuntamenti[_i].state = state;
                    cardToChangeFormat.appuntamenti[_i].number = _i;
                }
                cardToChangeFormat.appuntamenti = cardToChangeFormat.appuntamenti.Reverse().ToArray();
            }

            int indexPreApp = cardToChangeFormat.desc.IndexOf("APPUNTAMENTI:");
            if (indexPreApp != -1)
            {
                cardToChangeFormat.desc = cardToChangeFormat.desc.Substring(0, indexPreApp);
                if (cardToChangeFormat.desc == "")
                {
                    cardToChangeFormat.desc = "Nessuna nota.";
                }
            }
            #endregion

            return cardToChangeFormat;
        }




        public ActionTrello[] CommentFormatChange(ActionTrello[] actions)
        {
            for (int _i = 0; _i < actions.Length; _i++)
            {
                ActionTrello action = actions[_i];
                if (action.data.text.Contains(@"#"))
                {
                    //##APP
                    //#Venditore
                    action.data.text = action.data.text.Insert(0, "<b style=\"font-sizeof: larger;\">");
                    action.data.text = action.data.text.Insert(action.data.text.Length, "</b>");
                }

                if (action.data.text.Contains("@"))
                {
                    int repeat = action.data.text.Count(a => a.ToString().Contains("@"));
                    int offset = 0;
                    for (int i = 0; i < repeat; i++)
                    {
                        int tagStart = action.data.text.IndexOf("@", offset);
                        int tagEnd = tagStart;
                        bool tagFound = false;
                        while (!tagFound)
                        {
                            if (action.data.text.Length > tagEnd)
                            {
                                string letter = action.data.text.Substring(tagEnd, 1);
                                if (letter.Equals(" "))
                                {
                                    tagFound = true;
                                }
                                tagEnd++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        action.data.text = action.data.text.Insert(tagEnd, "</b>");
                        action.data.text = action.data.text.Insert(tagStart, "<b>");
                        offset = action.data.text.IndexOf("@", offset) + 1;
                    }
                }
            }
            return actions;
        }




        public async Task<Attachment[]> AttachmentFormatChange(IEnumerable<Claim> claims, Attachment[] attachments)
        { 
            List<Attachment> _dirty = attachments.ToList();
            for (int _i = 0; _i < _dirty.Count; _i++)
            {
                if (_dirty[_i].name.Equals("contatto_icon.png"))
                {
                    _dirty.RemoveAt(_i);
                    break;
                }
            }
            attachments = _dirty.ToArray();

            //Aggiungo informazioni sul creatore del commento
            for (int _i = 0; _i < attachments.Length; _i++)
            {
                attachments[_i].memberCreator = await GetMember(claims, attachments[_i].idMember);
            }
            return attachments;
        }




        public string GetVenditore(ActionTrello[] comments)
        {
            string venditore = "Nessuno.";
            foreach (ActionTrello action in comments)
            {
                if (action.data.text.Contains(@"#Venditore"))
                {
                    venditore = action.data.text.Replace(@"#Venditore", "");
                    break;
                }
            }

            return venditore;
        }




        public async Task<Card> GetCardData(IEnumerable<Claim> claims, Card card)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            card = await HttpGetCard(api_key, api_token, card.id);
            return card;
        }




        //Anche i dati dei plug-in/power-up
        public async Task<Card> GetCardFullData(IEnumerable<Claim> claims, Card card)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            card = await HttpGetCard(api_key, api_token, card.id);
            card.GetPluginValueCrmble();
            return card;
        }




        public async void AddLabel(IEnumerable<Claim> claims, string cardId, string labelBusiness)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            await HttpPostLabel(api_key, api_token, cardId, labelBusiness);
        }




        public async void RemoveLabel(IEnumerable<Claim> claims, string cardId, string labelBusiness)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            await HttpDeleteLabel(api_key, api_token, cardId, labelBusiness);
        }




        //Potrebbe resituire l'oggetto ActionTrello, ma attualmente non mi serve
        public async void AddComment(IEnumerable<Claim> claims, string cardId, string comment)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;
            await HttpPostComment(api_key, api_token, cardId, comment);
        }




        public async Task<ActionTrello[]> GetComments(IEnumerable<Claim> claims, string cardId)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;

            return await HttpGetActionByCardId(api_key, api_token, cardId, "commentCard");
        }




        public async Task<Attachment[]> GetAttachments(IEnumerable<Claim> claims, string cardId)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;

            return await HttpGetAttachments(api_key, api_token, cardId);
        }




        public async Task AddAttachment(IEnumerable<Claim> claims, Card card, string fileName, string url)
        {
            string api_key = claims.First(claim => claim.Type.Equals(User.API_KEY)).Value;
            string api_token = claims.First(claim => claim.Type.Equals(User.API_TOKEN)).Value;

            await HttpPostAttachment(api_key, api_token, card.id, fileName, url);
        }




        //Eliminabile
        public async Task AddAttachment(string api_key, string api_token, string cardId, string fileName, string url)
        {
            await HttpPostAttachment(api_key, api_token, cardId, fileName, url);
        }
        #endregion
    }
}
