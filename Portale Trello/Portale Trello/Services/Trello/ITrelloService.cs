﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;
using Portale_Trello.Models.Trello;
using Portale_Trello.Models.Trello.Actions;

namespace Portale_Trello.Services.Trello
{
    interface ITrelloService
    {
        #region Board
        Task<Label[]> HttpGetLabels(string api_key, string api_token);
        Task<Member> HttpGetMember(string api_key, string api_token, string id);
        #endregion

        #region List
        Task<List[]> HttpGetLists(string api_key, string token);
        Task<Card[]> HttpGetCardsPerList(string api_key, string token, string listId);
        #endregion

        #region Card
        Task<Card> HttpGetCard(string api_key, string api_token, string cardId);
        Task<ActionTrello[]> HttpGetActionByCardId(string api_key, string api_token, string cardId, string filter = "commentCard");
        Task HttpPutDueComplete(string api_key, string api_token, string cardId, bool is_complete);
        Task HttpPutDueDate(string api_key, string api_token, string cardId, string due, string start);
        Task HttpPostLabel(string api_key, string api_token, string cardId, string labelBusiness);
        Task HttpPostComment(string api_key, string api_token, string cardId, string comment);
        Task HttpDeleteLabel(string api_key, string api_token, string cardId, string labelBusiness);
        Task<Attachment[]> HttpGetAttachments(string api_key, string api_token, string cardId);
        Task HttpPostAttachment(string api_key, string api_token, string cardId, string fileName, string url);
        #endregion
    }
}