﻿using System.Net.Http.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Portale_Trello.Models.Etc;
using Portale_Trello.Models.Trello;
using Portale_Trello.Models.Trello.Actions;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Components.Forms;
using System.Collections.Generic;

namespace Portale_Trello.Services.Trello
{
    public class TrelloService : ITrelloService
    {
        protected readonly HttpClient httpClient;

        /*public TrelloService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }*/

        public TrelloService()
        {
            if (httpClient == null)
            {
                httpClient = new HttpClient();
                httpClient.BaseAddress = new Uri("https://api.trello.com/1/");
            }
        }




        #region Board
        public async Task<Label[]> HttpGetLabels(string api_key, string api_token)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per ottenere le lables presenti in board");
            return await httpClient.GetFromJsonAsync<Label[]>("boards/" + TrelloContext.boardId + "/labels?key=" + api_key + "&token=" + api_token);
        }

        public async Task<Member> HttpGetMember(string api_key, string api_token, string memberId)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per ottenere le informazioni del membro id: " + memberId);
            return await httpClient.GetFromJsonAsync<Member>("members/" + memberId + "?key=" + api_key + "&token=" + api_token);
        }
        #endregion




        #region List
        public async Task<List[]> HttpGetLists(string api_key, string api_token)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per ottenere le liste presenti in bacheca");
            return await httpClient.GetFromJsonAsync<List[]>("boards/qsSRWiRN/lists?key=" + api_key + "&token=" + api_token);
        }




        public async Task<Card[]> HttpGetCardsPerList(string api_key, string api_token, string listId)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per ottenere le schede contenute nella lista id = " + listId);
            return await httpClient.GetFromJsonAsync<Card[]>("lists/" + listId + "/cards?key=" + api_key + "&token=" + api_token);
        }
        #endregion




        #region Card
        //N.B. pluginData=true per ottenere anche tutti i dati di tutti i plugin presenti nella scheda.
        public async Task<Card> HttpGetCard(string api_key, string api_token, string cardId)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per ottenere una scheda tramite suo id = " + cardId);
            return await httpClient.GetFromJsonAsync<Card>("cards/" + cardId + "?pluginData=true&key=" + api_key + "&token=" + api_token);
        }




        public async Task<ActionTrello[]> HttpGetActionByCardId(string api_key, string api_token, string cardId, string filter = "commentCard")
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per leggere le Azioni (Commenti ed altro) delle scheda id: " + cardId);
            return await httpClient.GetFromJsonAsync<ActionTrello[]>("cards/" + cardId + "/actions?filter=" + filter + "&key=" + api_key + "&token=" + api_token);
        }




        public async Task HttpPutDueComplete(string api_key, string api_token, string cardId, bool is_complete)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per impostare lo stato della schedenza su = " + is_complete + " per la scheda id = " + cardId);
            HttpResponseMessage response = await httpClient.PutAsync("cards/" + cardId + "?dueComplete=" + is_complete.ToString().ToLower() + "&key=" + api_key + "&token=" + api_token, new StringContent(""));
            response.EnsureSuccessStatusCode();
        }




        public async Task HttpPutDueDate(string api_key, string api_token, string cardId, string due, string start)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per impostare la data di scadenza con start: " + start + " e end: " + due + " per la scheda id: " + cardId);
            HttpResponseMessage response = await httpClient.PutAsync("cards/" + cardId + "?due=" + due + "&start=" + start + "&key=" + api_key + "&token=" + api_token, new StringContent(""));
            response.EnsureSuccessStatusCode();
        }




        public async Task HttpPostLabel(string api_key, string api_token, string cardId, string labelBusiness)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per aggiungere un'etichetta con id: " + labelBusiness + " alla scheda id: " + cardId);
            HttpResponseMessage response = await httpClient.PostAsync("cards/" + cardId + "/idLabels?value=" + labelBusiness + "&key=" + api_key + "&token=" + api_token, new StringContent(""));
            response.EnsureSuccessStatusCode();
        }




        //Potrebbe resituire l'oggetto ActionTrello, ma attualmente non mi serve
        public async Task HttpPostComment(string api_key, string api_token, string cardId, string comment)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per aggiungere un commento alla scheda id: " + cardId);
            HttpResponseMessage response = await httpClient.PostAsync("cards/" + cardId + "/actions/comments?text=" + comment + "&key=" + api_key + "&token=" + api_token, new StringContent(""));
            response.EnsureSuccessStatusCode();
        }




        public async Task HttpDeleteLabel(string api_key, string api_token, string cardId, string labelBusiness)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per rimuovere un'etichetta con id: " + labelBusiness + " alla scheda id: " + cardId);
            HttpResponseMessage response = await httpClient.DeleteAsync("cards/" + cardId + "/idLabels/" + labelBusiness + "?key=" + api_key + "&token=" + api_token);
            response.EnsureSuccessStatusCode();
        }




        public async Task<Attachment[]> HttpGetAttachments(string api_key, string api_token, string cardId)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per ricevere gli allegati della scheda id: " + cardId);
            return await httpClient.GetFromJsonAsync<Attachment[]>("cards/" + cardId + "/attachments?key=" + api_key + "&token=" + api_token); ;
        }




        public async Task HttpPostAttachment(string api_key, string api_token, string cardId, string fileName, string url)
        {
            ILogger logger = Logger.Singleton();
            logger.Log("Chiamata API Trello per inviare un allegato alla scheda id: " + cardId);
            HttpResponseMessage response = await httpClient.PostAsync("cards/" + cardId + "/attachments?name=" + fileName + "&url=" + url + "&key=" + api_key + "&token=" + api_token, new StringContent(""));
            response.EnsureSuccessStatusCode();
        }
        #endregion
    }
}