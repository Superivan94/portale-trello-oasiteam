using System;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Radzen;
using Portale_Trello.Data;
using Portale_Trello.Models.Etc;
using Portale_Trello.Models.Hubs;
using Portale_Trello.Models.Trello;
using Portale_Trello.Services.Trello;


namespace Portale_Trello
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            //Per creare un file di log personalizzato
            Logger.Singleton().Log("App Started.");
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();


            //Esempio servizio di default con nuovo progetto Blazor
            services.AddSingleton<WeatherForecastService>();


            //Servizio per autenticazione con Coockies
            /*
            Il Coockie creato in questo modo con lo scopo di autenticazione ha la propriet� IsEssential impostata con valore True.
            I coockies di  autenticazione sono permessi anche quando l'utente non ha dato il consenso per collezionare dati personali.

            For more information, see General Data Protection Regulation(GDPR) support in ASP.NET Core. Link:
            https://docs.microsoft.com/en-us/aspnet/core/security/gdpr?view=aspnetcore-5.0#essential-cookies
            */
            //Quando lo schema di autenticazione coockie (Authentication Scheme) non viene specificato in AddCoockie() verr� usatato come default CookieAuthenticationDefaults.AuthenticationScheme ("Cookies").
            //Per AddCoockie() potrei specificare delle opzioni/parametri ma per ora usa la configurazione di default
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();

            //Parte2
            // From: https://github.com/aspnet/Blazor/issues/1554
            // HttpContextAccessor
            services.AddHttpContextAccessor();
            services.AddScoped<HttpContextAccessor>();
            services.AddHttpClient();
            services.AddScoped<HttpClient>();


            //Servizio per Trello REST API, utilizzabile nelle pagine tramite @inject
            services.AddHttpClient<ITrelloService, TrelloService>(client =>
            {
                client.BaseAddress = new Uri("https://api.trello.com/1/");
            });
            services.AddSingleton<TrelloContext>();


            //Servizi Radzor
            services.AddScoped<DialogService>();
            //Notifiche a scomparsa https://blazor.radzen.com/notification
            services.AddScoped<NotificationService>();
            services.AddScoped<TooltipService>();
            services.AddScoped<ContextMenuService>();

            //Servizio per SignalR
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //Per SignalR
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            //Configurazione per autenticazione con Cookies
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapHub<MessagesHub>(AppEndPoints.MESSAGE_HUB);//SignalR "/messagesHub"
                endpoints.MapFallbackToPage(AppEndPoints.HOST);//"/_Host"

                //Aggiunti con la configurazione per autenticazione con Cookies
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });

            //Configarzione per autenticazione con Cookies
            app.UseCookiePolicy();
        }
    }
}