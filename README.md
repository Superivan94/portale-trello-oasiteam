# Trello Custom View - OasiTeam

Creazione di una interfaccia più avanzata per la gestione ed analisi dei dati rispetto all'interfaccia web nativa, utilizzando le risorse REST messe a disposizione da Trello.

---

Pacchetti installati tramite **NuGet** di Visual Studio sono:

- Per semplificare la gestione delle richieste REST:
	- *Microsoft.AspNetCore.Blazor.HttpClient* <--- Deprecato
	- *System.Net.Http.Json* <--- Sostituito da

Per espandere l'interfaccia grafica, uso un **framework** che implementa uso di Bootstrap e simili.
I due principali framework tra cui scegliere sono **Blazorise o Radzen.**
Blazorise usa Bootstrap e altre versioni leggermente modificate sotto altri nomi, meno componenti rispetto a Radzen.
Radzen sembra piu' completo.
Ho scelto Radzen.
- *Blazorise.Bootstrap*
- *Radzen.Blazor*

Per avvisare tutti i client collegati di un cambiamento invio dei messaggi che forzano l'aggiornamento dell view tramite il pacchetto
- *Microsoft.AspNetCore.SignalR.Client*

---
# Alcune linee guida per Blazor

Indice:
- 1 **Blazor**
	- 1.1 Aggiungere una pagina
	- 1.2 Chiamate REST API semplificate
	- 1.3 SignIn/Out Coockies
		- 1.3.1 Configurare Cookies di Autenticazione
		- 1.3.2 Blazor Server-Side Cookies
		- 1.3.3 Creazione Cookie di Autenticazione
	- 1.4 SignalR "Broadcast" Message/Event


- 2 **Radzen**
	- 2.1 Prima configurazione

---
### 1.1 Aggiungere una pagina
Bisogna creare un file con estensione razor **nome_file.razor.**
Solitamente va collocato nella directory *Pages*.
**Esempio di una pagina:**
```javascript
@page "/my_example_page"

@using Trello_Custom_View.Services.Trello
@inject ITrelloService trelloService
@inject HttpClient httpClient

<h1>Liste di Trello</h1>
<p>Questo componente dimostra il caricamento di dati da un servizio.</p>


@if (list == null)
{
    <p><em>Loading...</em></p>
}
else
{
    <table class="table">
        <thead>
            <tr>
                <th>Nome</th>
                <th>ID</th>
                <th>Board ID</th>
                <th>Seguendo</th>
            </tr>
        </thead>
        <tbody>
            @foreach (var l in list)
            {
                <tr>
                    <td>@l.name</td>
                    <td>@l.id</td>
                    <td>@l.idBoard</td>
                    <td>@l.subscribed</td>
                </tr>
            }
        </tbody>
    </table>
}


@code {
    private Trello_Custom_View.Models.Trello.List[] list;
    protected override async Task OnInitializedAsync()
    {
        list = await trelloService.GetList();
    }
}
```
La funzione **OnInitializedAsync()** viene sovrascritta, esistono altre funzioni in cui è prevista una sovrascrittura, questa precisamente viene chiamata al caricamento della della pagina. La modalità multitasking permette una chiamata a delle API REST in modalità *asincrona*. Infatti finché non viene valorizzata la variabile *list* viene mostrato un altro messaggio, come si può vedere nella parte iniziale dell'*HTML*.

Per rendere la pagina disponibile nel **Menù di navigazione** dobbiamo localizzare il file *NavMenu.razor* solitamente posizionato nella directory *Shared*.
Dobbiamo aggiungere questo componente alla lista *HTML*
```javascript
<li class="nav-item px-3">
	<NavLink class="nav-link" href="trello_list">
		<span class="oi oi-list-rich" aria-hidden="true"></span> Trello List
	</NavLink>
</li>
```
Il codice di *default* di questo componente si presenta così:
```javascript
<div class="top-row pl-4 navbar navbar-dark">
    <a class="navbar-brand" href="">Trello Custom View</a>
    <button class="navbar-toggler" @onclick="ToggleNavMenu">
        <span class="navbar-toggler-icon"></span>
    </button>
</div>

<div class="@NavMenuCssClass" @onclick="ToggleNavMenu">
    <ul class="nav flex-column">
        <li class="nav-item px-3">
            <NavLink class="nav-link" href="" Match="NavLinkMatch.All">
                <span class="oi oi-home" aria-hidden="true"></span> Home
            </NavLink>
        </li>
        <li class="nav-item px-3">
            <NavLink class="nav-link" href="counter">
                <span class="oi oi-plus" aria-hidden="true"></span> Counter
            </NavLink>
        </li>
        <li class="nav-item px-3">
            <NavLink class="nav-link" href="fetchdata">
                <span class="oi oi-list-rich" aria-hidden="true"></span> Fetch data
            </NavLink>
        </li>
        <li class="nav-item px-3">
            <NavLink class="nav-link" href="trello_list">
                <span class="oi oi-list-rich" aria-hidden="true"></span> Trello List
            </NavLink>
        </li>
    </ul>
</div>

@code {
    private bool collapseNavMenu = true;

    private string NavMenuCssClass => collapseNavMenu ? "collapse" : null;

    private void ToggleNavMenu()
    {
        collapseNavMenu = !collapseNavMenu;
    }
}
```
### 1.2 Chiamate REST API semplificate
Per semplificare la transcodifica della risposta REST solitamente in *JSON* ad i nostri modelli (oggetti del linguaggio di programmazione) ho installato il pacchetto *Microsoft.AspNetCore.Blazor.HttpClient* nominato all'inizio di questo documento.

Questo componente funziona tramite *Injection*. Bisogna, di conseguenza, prima definire i servizi, in questo caso dei client dove ci risponderanno alle nostre chiamate. Per fare questo dobbiamo trovare il file *Startup.cs* che di default possiede 3 metodi:

- *public Startup(IConfiguration configuration)*
```javascript
public Startup(IConfiguration configuration)
```

- *ConfigureServices()*
```javascript
public void ConfigureServices(IServiceCollection services)
```

- *Configure()*
```javascript
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
```

- **Startup** è una injection delle configurazioni.

- **ConfigureServices** è quella di nostro principale interesse dove andremo aggiungere i client e *URI* per le nostre chiamate REST.

- **Configure** è dove vengono gestiti alcuni comportamenti generici dell'applicazione. Per esempio vengono impostati se visualizzare una pagina di *errore* oppure lo *ErrorStackTrace*, questo comportamento si basa sul file di impostazioni che è stato *injected* in questo contesto tramite la funzione *Startup()*.
- Due file coinvolti in questo processo di esempio sono:
- *launchSettings.json*
- *appsettings.json*

La funzione di nostro interesse è *ConfigureServices* dove andremo a configurare il servizio *HttpClient* per la chiamata REST.
```javascript
// This method gets called by the runtime. Use this method to add services to the container.
// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
public void ConfigureServices(IServiceCollection services)
{
	services.AddRazorPages();
	services.AddServerSideBlazor();
	//Servizio che esegue della logica in locale, poi viene iniettato nelle pagine in cui serve con @inject WeatherForecastService ForecastService
	services.AddSingleton<WeatherForecastService>();
	//Servizio REST con URI principale per accedere ai servizi REST, anche questo viene viene iniettato nelle pagine in cui necessario con il servizio @inject ITrelloService trelloService e il componente di aiuto per la traduzione di una risposta JSON @inject HttpClient httpClient
	services.AddHttpClient<Trello_Custom_View.Services.Trello.ITrelloService, Trello_Custom_View.Services.Trello.TrelloService>(client =>
	{
		client.BaseAddress = new Uri("https://api.trello.com/");
	});
}
```
Il servizio *WeatherForecastService* esegue della logica in locale, poi viene iniettato nelle pagine in cui serve con:
```javascript
@inject WeatherForecastService ForecastService
```
Il servizio *TrelloService* viene creato implementando l'interfaccia *ITrelloService* e definiendo *URI* base per il client che fornisce le risposte REST. Questo servizio viene iniettato con:
```javascript
@inject ITrelloService trelloService
```
e il componente di aiuto per la traduzione di una risposta JSON
```javascript
@inject HttpClient httpClient
```

### 1.3 SignIn/Out Coockies

Per abilitare un servizio di autenticazione nell'app possiamo usare Identity che è un *API* che si collega ad un database SQL Server di Microsoft. Offre molte funzionalità come l'autenticazione in due fattori, ma per un approccio più semplice e classico useremo l'autenticazione tramite *coockies*. In questo modo possiamo utilizzare il tipo di database che vogliamo, per esempio **MySQL.**


	Link per la documentazione ufficiale Microsoft:
	https://docs.microsoft.com/en-us/aspnet/core/security/authentication/cookie?view=aspnetcore-3.1

### 1.3.1 Configurare Cookies di Autenticazione

Nel file Startup.cs nel metodo dedicato alla *configurazione dei servizi* creiamo il servizio *Middleware* che gestirà e creerà i coockies utili per l'autenticazione:

```javascript
services
.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
.AddCookie();
```

Quando lo schema di autenticazione coockie (*Authentication Scheme*) non viene specificato in *AddCoockie()* verrà usatato come default CookieAuthenticationDefaults.AuthenticationScheme ("Cookies").

Il Coockie creato in questo modo con lo scopo di autenticazione ha la *proprietà* **IsEssential** impostata con valore **True.** I *coockies di  autenticazione* sono permessi anche quando l'utente non ha dato il consenso per collezionare dati personali.

	For more information, see General Data Protection Regulation (GDPR) support in ASP.NET Core. Link:
	https://docs.microsoft.com/en-us/aspnet/core/security/gdpr?view=aspnetcore-5.0#essential-cookies

Sempre in Startup.cs nel metodo *Configure* bisogna chiamare i metodi *UseAuthorization e UseAuthentication* per impostare la proprietà *User* nel contesto *HttpContext* (*HttpContext.User*). In questo modo si potrà utilizzare il Middleware per l'autenticazione.

I due metodi vanno chiamati prima della chiamata agli *Endpoints*:

```javascript
app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
	endpoints.MapBlazorHub();
	endpoints.MapFallbackToPage("/_Host");
	endpoints.MapControllers();
	endpoints.MapRazorPages();
});
```

È possibile specificare ulteriori parametri con la classe oggetto *CookieAuthenticationOptions*.

Imposta *CookieAuthenticationOptions* nella configurazione dei servizi di Startup.cs nel metodo Startup.ConfigureServices:

```javascript
services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        ...
    });
```

	Link alla documentazione per *CookieAuthenticationOptions*:
	https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.authentication.cookies.cookieauthenticationoptions?view=aspnetcore-5.0

Un ulteriore configurazione da usare è *Cookie Policy Middleware* che permette di gestire i criteri di ogni cookies:

```javascript
app.UseCookiePolicy(cookiePolicyOptions);
```

Anche per questa configurazione è possibile specificare alcune opzioni/parametri con la classe *MinimumSameSitePolicy*, se non specificato il valore di default sarà *SameSisteMode.Lax*:

```javascript
var cookiePolicyOptions = new CookiePolicyOptions
{
    MinimumSameSitePolicy = SameSiteMode.Strict,
};
```

		Link alla sezione nella documentazione ufficiale:
		https://docs.microsoft.com/en-us/aspnet/core/security/authentication/cookie?view=aspnetcore-5.0#cookie-policy-middleware

### 1.3.2 Blazor Server-Side Cookies

A quanto pare Blazor Server-Side "non ha la capacità nativa" di poter impostare dei *Cookies* nel *Browser Client* perché il codice che sarebbe destinato all'esecuzione del client viene in realtà eseguito dal server per motivi di sicurezza. Per aggirare questo problema, nella configurazione abbiamo mappato l'endpoint *Controllers (endpoint.MapControllers):*

```javascript
app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
	endpoints.MapBlazorHub();
	endpoints.MapFallbackToPage("/_Host");
	endpoints.MapControllers();
	endpoints.MapRazorPages();
});
```

Infine dobbiamo creare un *API Controller - Empty* in questo modo abbiamo esposti dei metodi che ci permettono di restituire informazioni al nostro *Browser Client*.
Esempio della risorsa necessaria per un *SignIn*:

```javascript
using System.Security.Claims;
using System.Threading.Tasks;
using Trello_Custom_View.Models.Etc;

namespace Trello_Custom_View.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class CookieController : ControllerBase
    {
        [HttpPost]
        public async Task<ActionResult> SinginUser([FromForm] string username, [FromForm] string password)
        {

            User user = await ResourceAccessorDatabase.Singleton().SingInUser(username, password);

            List<Claim> claims = CookieAuth.BuildClaim(user);
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            AuthenticationProperties authenticationProperties = new AuthenticationProperties();

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authenticationProperties);

            return Ok();
        }
    }
}
```

Per permettere a Blazor di utilizzare i sistemi di autenticazione bisogna dichiarare l'utilizzo del componente **CascadingAuthenticationState** nel file **App.razor**.

```javascript
<CascadingAuthenticationState>
    <Router AppAssembly="@typeof(Program).Assembly" PreferExactMatches="@true">
        <Found Context="routeData">
            <RouteView RouteData="@routeData" DefaultLayout="@typeof(MainLayout)" />
        </Found>
        <NotFound>
            <LayoutView Layout="@typeof(MainLayout)">
                <p>Sorry, there's nothing at this address.</p>
            </LayoutView>
        </NotFound>
    </Router>
</CascadingAuthenticationState>
```

Infine nelle *View* per accedere allo scheda di autenticaizone cookie bisogna utilizzare il componente **AuthorizeView**, **Authorized** e **NotAuthorized**:

```javascript
<h1>Login TrelloCustomView</h1>
<p>Questo componente permette il login.</p>
<AuthorizeView>
    <Authorized>
        <p>Ciao @context.User.Claims.First().Value</p>
    </Authorized>
    <NotAuthorized>
        <p>Non sei loggato!</p>
    </NotAuthorized>
</AuthorizeView>
```

### 1.3.3 Creazione Cookie di Autenticazione

Per creare e mantenere le informazioni di un utente nel coockie, bisogna costruire/istanziare una *ClaimsPrincipal*, che racchiuderà le informazioni serializzate nel cookie. Quindi creare una *ClaimsIdentity* con qualsivoglia *Claims* e successivamente chiamare il metodo *SingInAsync* per loggare l'utente:

```javascript
var claims = new List<Claim>
{
    new Claim(ClaimTypes.Name, user.Email),
    new Claim("FullName", user.FullName),
    new Claim(ClaimTypes.Role, "Administrator"),
};

var claimsIdentity = new ClaimsIdentity(
    claims, CookieAuthenticationDefaults.AuthenticationScheme);

var authProperties = new AuthenticationProperties
{
    //AllowRefresh = <bool>,
    // Refreshing the authentication session should be allowed.

    //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
    // The time at which the authentication ticket expires. A
    // value set here overrides the ExpireTimeSpan option of
    // CookieAuthenticationOptions set with AddCookie.

    //IsPersistent = true,
    // Whether the authentication session is persisted across
    // multiple requests. When used with cookies, controls
    // whether the cookie's lifetime is absolute (matching the
    // lifetime of the authentication ticket) or session-based.

    //IssuedUtc = <DateTimeOffset>,
    // The time at which the authentication ticket was issued.

    //RedirectUri = <string>
    // The full path or absolute URI to be used as an http
    // redirect response value.
};

await HttpContext.SignInAsync(
    CookieAuthenticationDefaults.AuthenticationScheme,
    new ClaimsPrincipal(claimsIdentity),
    authProperties);
```

### 1.4 SignalR "Broadcast" Message/Event

Documentazione ufficiale: https://docs.microsoft.com/en-us/aspnet/core/tutorials/signalr-blazor?view=aspnetcore-5.0&tabs=visual-studio&pivots=server

SignalR molto semplicemente permette di creare un punto comune nel server definito come "*Hub*". A questo *Hub* si sottoscrivono i client e possono scambiarsi dei messaggi a vicenda e/o rimanere in ascolto.

Per esempio:
```
Client1 e Client2 sono collegati a Server.

Client1 e Client2 visualizzano un valore.

Client1 inserisce un nuovo valore e manda un messaggio tramite SignalR a Server

Server riceve il messaggio e lo inoltra a tutti i client oppure a quelli che si sono sottoscritti a questo tipo di messaggio.

Client2 che è sottoscritto riceve il messaggio e aggiorna la view mostrando il nuovo valore.
```

Dopo aver installato il pacchetto tramite il gestore di pacchetti **NuGet** integrato in visual studio viene consigliato di creare una cartella dal nome plurale "*Hubs*". Seguendo un esempio di applicazione di chat, all'interno della cartella viene creato un oggetto chiamato "ChatHub" che deriva dall'oggetto padre "**Hub**"

```javascript
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace BlazorServerSignalRApp.Server.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}
```

Successivamente aggiungiamo il servizio *Response Compression Middleware* nel file *Startup.cs* nel metodo *ConfigureServices*, ecco un esempio su come dovrebbe apparire:

```javascript
public void ConfigureServices(IServiceCollection services)
{
    services.AddRazorPages();
    services.AddServerSideBlazor();
    services.AddSingleton<WeatherForecastService>();
    services.AddResponseCompression(opts =>
    {
        opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
            new[] { "application/octet-stream" });
    });
}
```

Invece sempre nel file *Startup* nel metodo *Configure* è **importante** aggiungere in **cima** *app.UseResponseCompression();* mentre al fondo aggiungiamo gli *ends points* come nell'esempio che segue:

```javascript
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    app.UseResponseCompression();//Attenzione qui

    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }
    else
    {
        app.UseExceptionHandler("/Error");
        app.UseHsts();
    }

    app.UseHttpsRedirection();
    app.UseStaticFiles();

    app.UseRouting();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapBlazorHub();
        endpoints.MapHub<ChatHub>("/chathub");//Attenzione qui
        endpoints.MapFallbackToPage("/_Host");
    });
}
```

Nella classe in cui viene implementato *SignalR* bisogna importare:

```javascript
@using Microsoft.AspNetCore.SignalR.Client
```

Ed implementare la liberazione delle risorse e rilascio della conessione:

```javascript
@implements IAsyncDisposable

public async ValueTask DisposeAsync()
{
		if (hubConnection is not null)
		{
				await hubConnection.DisposeAsync();
		}
}
```

Per registrarsi hai messaggi di *SignalR* bisogna iscriversi come nell'esempio:

```javascript
protected override async Task OnInitializedAsync()
{
		hubConnection = new HubConnectionBuilder()
				.WithUrl(NavigationManager.ToAbsoluteUri("/chathub"))
				.Build();

		hubConnection.On<string, string>("ReceiveMessage", (user, message) =>
		{
				var encodedMsg = $"{user}: {message}";
				messages.Add(encodedMsg);
				StateHasChanged();
		});

		await hubConnection.StartAsync();
}
```

Quando invece dobbiamo inviare un messaggio ad altri clients:

```javascript
public async Task Send()
{
		await hubConnection.SendAsync("SendMessage", userInput, messageInput);
}
```

---

### 2.1 Prima configurazione

https://blazor.radzen.com/get-started

---
Fare un capitolo
app in se partendo dal basso:
Database --> Databasa Access Layer CRUD --> Resource Accessor che comprende le macro operazioni da fare con il CRUD
